# def add_one(number):
#     return number + 1
#
#
# print(add_one(2))
#
#
# def say_hello(name):
#     return f"Hello {name}"
#
#
# def be_awesome(name):
#     return f"Yo {name}, together we are the awesomest!"
#
#
# def greet_bob(greeter_func):
#     return greeter_func("Bob")


# greet_bob(say_hello)


# def f1(func):
#     def wrapper():
#         print("Start")
#         func()
#         func()
#         print("End")
#
#     return wrapper
#
#
# @f1
# def hi():
#     print("Hello World!")\
#
# hi()

import functools
import time


def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time = end_time - start_time    # 3
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer


@timer
def waste_some_time(num_times):
    for _ in range(num_times):
        sum([i**2 for i in range(10000)])
